const express = require('express');
const http = require('http');
const socketIO = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

// Configura le rotte del server Express

io.on('connection', (socket) => {
  console.log('Nuova connessione: ' + socket.id);

  // Gestisci l'evento di invio di un nuovo messaggio
  socket.on('inviaMessaggio', (messaggio) => {
    // Emetti il messaggio a tutti gli altri client connessi
    socket.broadcast.emit('nuovoMessaggio', messaggio);
  });

  // Gestisci l'evento di disconnessione
  socket.on('disconnect', () => {
    console.log('Connessione terminata: ' + socket.id);
  });
});

// Avvia il server
server.listen(3001, () => {
  console.log('Server in ascolto sulla porta 3001');
});