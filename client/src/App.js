import React, { useState, useEffect } from 'react';
import io from 'socket.io-client';

const socket = io(`http://${process.env.REACT_APP_IP}:3001`);

function App() {
  const [messaggi, setMessaggi] = useState([]);
  const [nuovoMessaggio, setNuovoMessaggio] = useState('');

  useEffect(() => {
    // Gestisci l'evento per i nuovi messaggi ricevuti
    socket.on('nuovoMessaggio', (messaggio) => {
      setMessaggi([...messaggi, messaggio]);
    });

    return () => {
      // Pulisci i listener degli eventi quando il componente viene smontato
      socket.off('nuovoMessaggio');
    };
  }, [messaggi]);

  useEffect(() => {
    console.log(process.env.REACT_APP_IP);
  })

  const inviaMessaggio = () => {
    socket.emit('inviaMessaggio', nuovoMessaggio);
    setNuovoMessaggio('');
  };

  return (
    <div>
      <div>
        {messaggi.map((messaggio, index) => (
          <div key={index}>{messaggio}</div>
        ))}
      </div>
      <input
        type="text"
        value={nuovoMessaggio}
        onChange={(e) => setNuovoMessaggio(e.target.value)}
      />
      <button onClick={inviaMessaggio}>Invia</button>
    </div>
  );
}

export default App;